#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

struct Mastermind
{
    int sequence[4];
    int nbLives;
    int nbGamesWon = 0;
    int nbGamesLost = 0;
    int sequenceTest[4];
    int sequenceTry[4];
    int nbFound = 0;
    int nbOddlyPlaced = 0;
};

int askAndTest()
{
    int nbATest = 0;

    cin >> nbATest;

    while (nbATest < 0 || nbATest > 9)
    {
        cout << "Entrez une valeur entre 0 et 9" << endl;
        cin >> nbATest;
    }
    return nbATest;
}

int main()
{
    srand(time(NULL));
    Mastermind game;
    int newGame = 1;

    // On boucle tant que le flag est sur vrai, on en sort sur faux, cad quand l'utilisateur choisi de ne pas recommencer en fin de partie
    while (newGame == 1)
    {
        game.nbLives = 10; //initialise le nombre de vies

        cout << "L'ordinateur a choisi sa combinaison de 4 chiffres de 0 a 9, a vous de la deviner !" << endl;

        // i de 0 � 3 pour avoir 4 valeurs
        for (int i = 0 ; i < 4; ++i)
        {
            game.sequence[i] = rand()%10; // random de 0 a 9
        }

        // Tour de jeu. On boucle tant que l'on a encore des essais ou que l'on a pas gagn�
        while (game.nbLives != 0 && (game.sequenceTry[0] != -1 || game.sequenceTry[1] != -1 || game.sequenceTry[2] != -1 || game.sequenceTry[3] != -1))
        {
            // Remise � 0 des compteurs
            game.nbFound = 0;
            game.nbOddlyPlaced = 0;

            cout << "Essai numero : " << (11-game.nbLives) << endl;

            for (int i = 0 ; i < 4 ; ++i)
            {
                // Essai joueur
                game.sequenceTry[i] = askAndTest();
                game.sequenceTest[i] = game.sequence[i];

                // Test des nombres bien plac�s
                if (game.sequenceTry[i] == game.sequenceTest[i])
                {
                    game.nbFound ++;
                    game.sequenceTry[i] = -1; // en passant un nombre d�j� "trouv�" � -1 on s'assure qu'il ne soit pas retourn� positif deux fois
                    game.sequenceTest[i] = -2; //passe celui-ci � -2 pour �viter l'�galit� avec -1 dans le prochain test qui retournerait donc un faux positif
                }
            }

            cout << "Nombres bien places " << game.nbFound << endl;

            // Test des nombres pr�sents mais mal plac�s
            for (int i = 0 ; i < 4 ; ++i)
            {
                for (int j = 0 ; j < 4 ; ++j)
                {
                    if (game.sequenceTry[i] == game.sequenceTest[j])
                    {
                        game.nbOddlyPlaced ++;
                        game.sequenceTry[i] = -1;
                        game.sequenceTest[j] = -2;
                    }
                }
            }

            cout << "Nombres mal places " << game.nbOddlyPlaced << endl << endl;

            game.nbLives --;
        }

        if (game.sequenceTry[0] == -1 && game.sequenceTry[1] == -1 && game.sequenceTry[2] == -1 && game.sequenceTry[3] == -1)
        {
            cout << "Felicitations ! Vous gagnez" << endl;
            game.nbGamesWon ++;
        }
        else
        {
            cout << "Bah alors ! On est nul ?" << endl;
            game.nbGamesLost ++;
        }

        cout << "Parties gagnees " << game.nbGamesWon << endl;
        cout << "Parties perdues " << game.nbGamesLost << endl << endl;

        cout << "Voulez-vous faire une nouvelle partie ? (0) NON (1) OUI" << endl;

        cin >> newGame;
    }

    if (game.nbGamesLost > game.nbGamesWon)
    {
        cout << "COMPUTER WINS ! FATALITY !" << endl;
    }

    return 0;
}
